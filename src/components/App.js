import React, { Component } from 'react';

//Components
import Header from './Global/Header';
import Content from './Global/Content';
import Footer from './Global/Footer';

import items from '../data/menu';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header title ="App masajes Zetech" items ={items} />
        <Content/>
        <Footer copyright ="&copy Zetech 2019"/>
      </div>
    );
    // return (
    //   <div className="App">
    //   {/* <table class="table">
    //     <thead>
    //     </thead>
    //       <tbody>

    //       </tbody>
    //   </table> */}
    //      <Button
    //     renderAs="a"
    //     href="http://www.zetech.com.ar"
    //     color="success"
    //     size="large"
    //     rounded
    //     outlined>
    //     Vamos a Zetech
    //   </Button>
    //   <Columns>
    //     <Columns.Column size={1}>One</Columns.Column>
    //     <Columns.Column>Eleven</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={2}>Two</Columns.Column>
    //     <Columns.Column>Ten</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={3}>Three</Columns.Column>
    //     <Columns.Column>Nine</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={4}>Four</Columns.Column>
    //     <Columns.Column>Eight</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={5}>Five</Columns.Column>
    //     <Columns.Column>Seven</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={6}>Six</Columns.Column>
    //     <Columns.Column>Six</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={7}>Seven</Columns.Column>
    //     <Columns.Column>Five</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={8}>Eight</Columns.Column>
    //     <Columns.Column>Four</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={9}>Nine</Columns.Column>
    //     <Columns.Column>Three</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={10}>Ten</Columns.Column>
    //     <Columns.Column>Two</Columns.Column>
    //   </Columns>
    //   <Columns>
    //     <Columns.Column size={11}>Eleven</Columns.Column>
    //     <Columns.Column>One</Columns.Column>
    //   </Columns>
    //   </div>
    // )
  }
}
export default App;
