import React, { Component } from 'react';
import './css/Content.css';
import axios from 'axios';
// import { Button } from "react-bulma-components/full";
// import { Columns } from "react-bulma-components/full";

class Content extends Component {
  state = {
    persons: []
  }

  componentDidMount() {
    axios.get("https://jsonplaceholder.typicode.com/users")
      .then(res => {
        // console.log(res.data);
        const persons = res.data;
        this.setState({ persons });
      })
  }
  render() {
    return (
      <div className="Content">
        <h2> Render User</h2>
        { this.state.persons.map((person,key) => <li key={key} >{person.name} {person.username }</li>)}
      </div>
    );
  }
}
export default Content;
