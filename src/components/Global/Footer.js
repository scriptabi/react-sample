import React, { Component } from 'react';
import propTypes from 'prop-types';

import './css/Footer.css';

class Footer extends Component {
  static propTypes ={
    copyright: propTypes.string,
  }

  render() {
    const  {copyright ="&copy React 2019"} = this.props;

    return (
      <div className="Footer">
        <h4 dangerouslySetInnerHTML={{__html:copyright}}></h4>
      </div>
    );
  }
}
export default Footer;
